# khl4express
Version of the khl4 server using the express framework

Start the server with `npm start`

If you want to start the server while the pd clients are not available use `export NO_PD=true && npm start`
